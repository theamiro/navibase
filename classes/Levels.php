<?php
require_once ("BaseLogicClass.php");
class Levels {
	private $connection;
	public function __construct() {
		$BaseLogicClass = new BaseLogicClass ();
		$db = $BaseLogicClass->connect ();
		$this->connection = $db;
	}
	public function execute_query($sql) {
		$query = $this->connection->prepare ( $sql );
		return $query;
	}
	public function register($name, $description) {
		try {
			$query = $this->connection->prepare ( "INSERT INTO levels(name,description) VALUES(:name,:description)" );
			
			$query->bindParam ( ":name", $name );
			$query->bindParam ( ":description", $description );
			
			$query->execute ();
			
			return $query;
		} catch ( PDOException $e ) {
			echo $e->getMessage ();
		}
	}
	public function getList() {
		try {
			$query = $this->connection->prepare ( "SELECT * FROM levels WHERE 1=1 ORDER BY id" );
			$query->execute ();
			while ( $row = $query->fetch ( PDO::FETCH_ASSOC ) ) {
				$data [] = $row;
			}
			return $data;

		} catch ( PDOException $e ) {
			echo $e->getMessage ();
		}
	}
	public function is_Loggedin() {
		if (isset ( $_SESSION ['user_session'] )) {
			return true;
		}
	}
	public function redirect($url) {
		header ( "Location:$url" );
	}
	public function logout() {
		session_destroy ();
		unset ( $_SESSION ['user_session'] );
		return true;
	}
}
?>