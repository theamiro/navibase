<?php
class BaseLogicClass {
	private $host = "localhost";
	private $user = "root";
	private $pass = "haven";
	private $db_name = "navibase";
	public $connection;
	public function connect() {
		$this->connection = null;
		try {
			$this->connection = new PDO ( "mysql:host=" . $this->host . ";dbname=" . $this->db_name, $this->user, $this->pass );
			$this->connection->setAttribute ( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
		} catch ( PDOException $exception ) {
			echo "Connection error: " . $exception->getMessage ();
		}
		return $this->connection;
	}
}
?>