<?php
require_once ("session.php");
include_once ("../classes/Levels.php");
include_once ("../classes/Users.php");

$authorize_user = new Users ();

$username = $_SESSION ['user_session'];

$query = $authorize_user->execute_query ( "SELECT * FROM users where username = :username" );

$query->execute ( array (
		":username" => $username 
) );
$userRow = $query->fetch ( PDO::FETCH_ASSOC );

$new_level = new Levels ();
if (isset ( $_POST ['registerbtn'] )) {
	$name = strip_tags ( $_POST ['txtname'] );
	$description = strip_tags ( $_POST ['txtdescription'] );
	
	if ($name == "") {
		$error [] = "The Group Name cannot be empty!";
	} elseif ($description == "") {
		$error [] = "The Group Level cannot be empty!";
	} else {
		try {
			$query = $new_level->execute_query ( "SELECT * FROM levels WHERE name=:name" );
			$query->execute ( array (
					':name' => $name 
			) );
			$row = $query->fetch ( PDO::FETCH_ASSOC );
			if ($row ['name'] == $name) {
				$error [] = "A level by the name of <b>" . $name . "</b> already exists!";
			} else {
				if ($new_level->register ( $name, $description )) {
					$new_level->redirect ( 'levels.php?added' );
				}
			}
		} catch ( PDOException $e ) {
			echo $e->getMessage ();
		}
	}
}
?>
<!DOCTYPE html>
<html lang="en">
<?php
$page_title = "Levels";
include_once ("common_files/head.php");
?>
<body>
	<?php include_once ("common_files/nav.php");?>
	<!-- Page Content -->
	<div class="container">
		<ul class="nav nav-tabs" role="tablist">
			<li class="nav-item active" role="presentation"><a class="nav-link"
				data-toggle="tab" href="#add_levels" role="tab"> <span><i
						class="fa fa-plus"></i></span> Add Levels
			</a></li>
			<li class="nav-item" role="presentation"><a class="nav-link"
				data-toggle="tab" href="#levels" role="tab"> <span><i
						class="fa fa-list"></i></span> Listing <span class="badge">
						<?php
						include_once ("../classes/Levels.php");
						$current_levels = new Levels ();
						$current_list = $current_levels->getList ();
						echo count ( $current_list );
						?></span>
			</a></li>
		</ul>
		<!-- Tab panes -->
		<div class="tab-content">
			<div class="tab-pane fade in active" id="add_levels" role="tabpanel">
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-primary">
							<div class="panel-heading clearfix">
								<span class="pull-right"><i class="fa fa-users fa-3x"></i></span>
								<h4>ADD NEW LEVEL</h4>
							</div>
							<form method="POST">
								<div class="panel-body">
									<div class="row">
										<div class="col-md-6 col-md-offset-3">
									<?php
									if (isset ( $error )) {
										foreach ( $error as $error ) {
											?>
													<div class="alert alert-danger" role="alert" id="messages">
												<p>
														<?= $error;?>
														<span class="pull-right"><i
														class="fa fa-times-circle fa-lg"></i></span>
												</p>
											</div>
													<?php } }elseif (isset($_GET['added'])){?>
													<div class="alert alert-success" role="alert" id="messages">
												<p>
													Level has successfully been added! <span class="pull-right"><i
														class="fa fa-tick-circle fa-lg"></i></span>
												</p>
											</div>
													<?php }?>
								</div>
									</div>
									<div class="row">
										<div class="col-md-6 col-md-offset-3">
											<label>Level Name:</label>
											<div class="input-group">
												<span class="input-group-addon"><i class="fa fa-pencil"></i></span>
												<input type="text" class="form-control"
													value="<?php if (isset($error)){ echo $name;}?>"
													placeholder="Level Name" aria-describedby="basic-addon1"
													required autofocus name="txtname" id="txtname">
											</div>
											<br> <label>Description:</label>
											<div class="input-group">
												<span class="input-group-addon"><i class="fa fa-comment"></i></span>
												<textarea name="txtdescription" class="form-control"
													rows="3" cols=""><?php if (isset($error)){ echo $description;}?></textarea>
											</div>
											<br>
											<button class="btn btn-success pull-right" type="submit"
												name="registerbtn">
												<i class="fa fa-save fa-lg"></i> Save
											</button>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<div class="tab-pane fade" id="levels" role="tabpanel">
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-primary">
							<div class="panel-heading">
								<span class="pull-right"><i class="fa fa-group fa-3x"></i></span>
								<h4>LEVEL LISTINGS</h4>
							</div>
							<div class="panel-body">
								<div class="row">
									<div class="col-md-12">
										<div class="table-responsive">
											<table class="table table-striped table-bordered"
												id="datatable">
												<thead>
													<tr>
														<th>Level ID</th>
														<th>Name</th>
														<th>Description</th>
													</tr>
												</thead>
												<tbody>
							<?php
							include_once ("../classes/Levels.php");
							$current_levels = new Levels ();
							$current_list = $current_levels->getList ();
							if (sizeof ( $current_list ) > 0) {
								foreach ( $current_list as $current_item ) {
									?>
													<tr>
														<td><?php print $current_item['id'];?></td>
														<td><?php print $current_item['name'];?></td>
														<td><?php print $current_item['description'];?></td>
													</tr>
							<?php }}else{?>			
													<tr class="danger">
														<td colspan="3" align="center"><strong>No Entries Found</strong></td>
													</tr>
							<?php }?>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /.row -->
			</div>
		</div>
	</div>
	<!-- /.container -->
	<?php
	include_once ("common_files/javascript.php");
	?>
</body>
</html>
