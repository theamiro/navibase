<?php
require_once ("session.php");
include_once ("../classes/Users.php");
include_once ("../classes/Members.php");
$authorize_user = new Users ();

$username = $_SESSION ['user_session'];

$query = $authorize_user->execute_query ( "SELECT * FROM users where username = :username" );

$query->execute ( array (
		":username" => $username 
) );
$userRow = $query->fetch ( PDO::FETCH_ASSOC );

$new_member = new Members ();
if (isset ( $_POST ['registerbtn'] )) {
	$name = strip_tags ( $_POST ['txtname'] );
	$date_of_birth = strip_tags ( $_POST ['txtdate_of_birth'] );
	$gender = strip_tags ( $_POST ['txtgender'] );
	$school = strip_tags ( $_POST ['txtschool'] );
	$church = strip_tags ( $_POST ['txtchurch'] );
	$career = strip_tags ( $_POST ['txtcareer'] );
	$allergy = strip_tags ( $_POST ['txtallergy'] );
	$address = strip_tags ( $_POST ['txtaddress'] );
	$email = strip_tags ( $_POST ['txtemail'] );
	$phone = strip_tags ( $_POST ['txtphone'] );
	$other_phone = strip_tags ( $_POST ['txtother_phone'] );
	$parent_name = strip_tags ( $_POST ['txtparent_name'] );
	$parent_phone = strip_tags ( $_POST ['txtparent_phone'] );
	$parent_career = strip_tags ( $_POST ['txtparent_career'] );
	$parent_email = strip_tags ( $_POST ['txtparent_email'] );
	$level = strip_tags ( $_POST ['txtlevel'] );
	$group_name = strip_tags ( $_POST ['txtgroup_name'] );
	
	if ($name == "") {
		$error [] = "Name Cannot be empty!";
	} else {
		try {
			$query = $new_member->execute_query ( "SELECT * FROM members WHERE name=:name" );
			$query->execute ( array (
					':name' => $name 
			) );
			$row = $query->fetch ( PDO::FETCH_ASSOC );
			if ($row ['name'] == $name) {
				$error [] = "Member already exists!";
			} else {
				if ($new_member->register ( $name, $date_of_birth, $gender, $school, $church, $career, $allergy, $address, $email, $phone, $other_phone, $parent_name, $parent_phone, $parent_career, $parent_email, $level, $group_name )) {
					$new_member->redirect ( 'members.php?added' );
				}
			}
		} catch ( PDOException $e ) {
			echo $e->getMessage ();
		}
	}
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
<?php
$page_title = "Members";
include_once ("common_files/head.php");
?>
</head>
<body>
<?php include_once("common_files/nav.php");?>
	<!-- Page Content -->
	<div class="container">
		<ul class="nav nav-tabs" role="tablist">
			<li class="nav-item active" role="presentation"><a class="nav-link"
				data-toggle="tab" href="#addition_form" role="tab"> <span><i
						class="fa fa-plus"></i></span> Add Members
			</a></li>
			<li class="nav-item" role="presentation"><a class="nav-link"
				data-toggle="tab" href="#listing" role="tab"> <span><i
						class="fa fa-list"></i></span> Listing <span class="badge">
						<?php
						include_once ("../classes/Members.php");
						$current_members = new Members ();
						$current_list = $current_members->getList ();
						echo count ( $current_list );
						?></span>
			</a></li>
		</ul>
		<!-- Tab Panes -->
		<div class="tab-content">
			<div class="tab-pane fade in active" id="addition_form"
				role="tabpanel">
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-primary">
							<div class="panel-heading">
								<span class="pull-right"><i class="fa fa-user-plus fa-3x"></i></span>
								<h4>NEW MEMBER</h4>
							</div>
							<form method="post" action="">
								<div class="panel-body">
									<div class="row">
										<div class="col-md-12">
											<?php
											if (isset ( $error )) {
												foreach ( $error as $error ) {
													?>
													<div class="alert alert-danger" role="alert" id="messages">
												<p>
														<?= $error;?>
														<span class="pull-right"><i
														class="fa fa-times-circle fa-lg"></i></span>
												</p>
											</div>
													<?php } }elseif (isset($_GET['added'])){?>
													<div class="alert alert-success" role="alert" id="messages">
												<p>
													Member already exists! <span class="pull-right"><i
														class="fa fa-tick-circle fa-lg"></i></span>
												</p>
											</div>
													<?php }?>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="row">
												<div class="col-lg-12">
													<h4>Personal Information</h4>
												</div>
											</div>
											<div class="row">
												<div class="col-md-12">
													<div class="input-group">
														<span class="input-group-addon"><i class="fa fa-user"></i></span>
														<input type="text" class="form-control"
															value="<?php if (isset($error)){ echo $name;}?>"
															placeholder="Full Name" aria-describedby="basic-addon1"
															required autofocus name="txtname" id="txtname">
													</div>
													<br>
												</div>
												<div class="col-md-6">
													<div class="input-group">
														<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
														<input type="date" class="form-control"
															value="<?php if (isset($error)){ echo $date_of_birth;}?>"
															placeholder="Date of Birth"
															aria-describedby="basic-addon1" required
															name="txtdate_of_birth" id="txtdate_of_birth">
													</div>
													<br>
													<div class="input-group">
														<span class="input-group-addon"><i
															class="fa fa-venus-mars"></i></span><select
															class="form-control" name="txtgender" id="txtgender">
															<option selected="selected" style="display: none;">Select
																Gender</option>
															<option>Male</option>

															<option>Female</option>
														</select>
													</div>
													<br>
													<div class="input-group">
														<span class="input-group-addon"><i
															class="fa fa-university"></i></span> <input type="text"
															class="form-control"
															value="<?php if (isset($error)){ echo $school;}?>"
															placeholder="School" aria-describedby="basic-addon1"
															required name="txtschool" id="txtschool">
													</div>
													<br>
												</div>
												<div class="col-md-6">
													<div class="input-group">
														<span class="input-group-addon"><i class="fa fa-home"></i></span>
														<input type="text" class="form-control"
															value="<?php if (isset($error)){ echo $church;}?>"
															placeholder="Church" aria-describedby="basic-addon1"
															required name="txtchurch" id="txtchurch">
													</div>
													<br>
													<div class="input-group">
														<span class="input-group-addon"><i
															class="fa fa-line-chart"></i></span> <input type="text"
															class="form-control"
															value="<?php if (isset($error)){ echo $career;}?>"
															placeholder="Career/Ambition"
															aria-describedby="basic-addon1" required name="txtcareer"
															id="txtcareer">
													</div>
													<br>
													<div class="input-group">
														<span class="input-group-addon"><i class="fa fa-asterisk"></i></span>
														<input type="text" class="form-control"
															value="<?php if (isset($error)){ echo $allergy;}?>"
															placeholder="Allergy" aria-describedby="basic-addon1"
															required name="txtallergy" id="txtallergy">
													</div>
													<br>
												</div>
												<div class="col-md-6">
													<div class="input-group">
														<span class="input-group-addon"><i
															class="fa fa-map-marker"></i></span> <input type="text"
															class="form-control"
															value="<?php if (isset($error)){ echo $address;}?>"
															placeholder="Physical Address"
															aria-describedby="basic-addon1" required
															name="txtaddress" id="txtaddress">
													</div>
													<br>
													<div class="input-group">
														<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
														<input type="email" class="form-control"
															value="<?php if (isset($error)){ echo $email;}?>"
															placeholder="Email Address"
															aria-describedby="basic-addon1" required name="txtemail"
															id="txtemail">
													</div>
													<br>
												</div>
												<div class="col-md-6">
													<div class="input-group">
														<span class="input-group-addon"><i class="fa fa-phone"></i></span>
														<input type="text" class="form-control"
															value="<?php if (isset($error)){ echo $phone;}?>"
															placeholder="Telephone Number"
															aria-describedby="basic-addon1" required name="txtphone"
															id="txtphone">
													</div>
													<br>
													<div class="input-group">
														<span class="input-group-addon"><i class="fa fa-phone"></i></span>
														<input type="text" class="form-control"
															value="<?php if (isset($error)){ echo $other_phone;}?>"
															placeholder="Telephone Number (other)"
															aria-describedby="basic-addon1" required
															name="txtother_phone" id="txtother_phone">
													</div>
													<br>
												</div>
												<div class="col-md-12"></div>
											</div>

										</div>
										<div class="col-md-6">
											<div class="row">
												<div class="col-md-12">
													<h4>Parent/Guardian Information</h4>
												</div>
											</div>
											<div class="row">
												<div class="col-md-12">
													<div class="input-group">
														<span class="input-group-addon"><i
															class="fa fa-venus-mars"></i></span> <input type="text"
															class="form-control"
															value="<?php if (isset($error)){ echo $parent_name;}?>"
															placeholder="Parent/Guardian Name"
															aria-describedby="basic-addon1" required
															name="txtparent_name" id="txtparent_name">
													</div>
													<br>
													<div class="input-group">
														<span class="input-group-addon"><i class="fa fa-phone"></i></span>
														<input type="text" class="form-control"
															value="<?php if (isset($error)){ echo $parent_phone;}?>"
															placeholder="Telephone Number"
															aria-describedby="basic-addon1" required
															name="txtparent_phone" id="txtparent_phone">
													</div>
													<br>
													<div class="input-group">
														<span class="input-group-addon"><i
															class="fa fa-line-chart"></i></span> <input type="text"
															class="form-control"
															value="<?php if (isset($error)){ echo $parent_career;}?>"
															placeholder="Career" aria-describedby="basic-addon1"
															required name="txtparent_career" id="txtparent_career">
													</div>
													<br>
													<div class="input-group">
														<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
														<input type="email" class="form-control"
															value="<?php if (isset($error)){ echo $parent_email;}?>"
															placeholder="Parent/Guardian Email Address"
															aria-describedby="basic-addon1" required
															name="txtparent_email" id="txtparent_email">
													</div>
													<br>
												</div>
											</div>
											<div class="row">
												<div class="col-lg-12">
													<h4>Coregroup Information</h4>
												</div>
											</div>
											<div class="row">
												<div class="col-lg-6">
													<div class="input-group">
														<span class="input-group-addon"><i class="fa fa-signal"></i></span><select
															class="form-control" name="txtlevel" id="txtlevel">
															<option selected="selected" style="display: none;">Select
																Level</option>
																<?php
																include_once ("../classes/Levels.php");
																$current_levels = new Levels ();
																$current_list = $current_levels->getList ();
																if (sizeof ( $current_list ) > 0) {
																	foreach ( $current_list as $current_item ) {
																		?>
															<option><?php print $current_item['name'];?></option>
													<?php }}?>
														</select>
													</div>
													<br>
												</div>
												<div class="col-lg-6">
													<div class="input-group">
														<span class="input-group-addon"><i class="fa fa-group"></i></span><select
															class="form-control" name="txtgroup_name"
															id="txtgroup_name">
															<option selected="selected" style="display: none;">Select
																Group</option>
																<?php
																include_once ("../classes/Groups.php");
																$current_groups = new Groups ();
																$current_list = $current_groups->getList ();
																if (sizeof ( $current_list ) > 0) {
																	foreach ( $current_list as $current_item ) {
																		?>
															<option><?php print $current_item['name'];?></option>
													<?php }}?>
														</select>
													</div>
													<br>
												</div>
											</div>
										</div>

									</div>
									<div class="row">
										<div class="col-md-6 col-xs-6" align="left"></div>
										<div class="col-md-6 col-xs-6" align="right">
											<button class="btn btn-success" name="registerbtn"
												type="submit">
												<i class="fa fa-save fa-lg"></i> Save
											</button>

										</div>
									</div>

								</div>

							</form>
						</div>
					</div>
				</div>
				<!-- /.row -->
			</div>
			<div class="tab-pane fade" id="listing" role="tabpanel">
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-primary">
							<div class="panel-heading clearfix">
								<span class="pull-right"><i class="fa fa-users fa-3x"></i></span>
								<h4>MEMBERS LISTING</h4>
							</div>
							<div class="panel-body">
								<div class="table-responsive">
									<table id="datatable"
										class="table table-striped table-condensed table-bordered">
										<thead>
											<tr>
												<th>ID</th>
												<th>Name</th>
												<th>Birthday</th>
												<th>School</th>
												<th>Church</th>
												<th>Career</th>
												<th>Allergy</th>
												<th>Address</th>
												<th>Email</th>
												<th>Phone</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
										<?php
										include_once ("../classes/Members.php");
										$current_members = new Members ();
										$current_list = $current_members->getList ();
										if (sizeof ( $current_list ) > 0) {
											foreach ( $current_list as $current_item ) {
												?>
											<tr>
												<td><?php print $current_item['id'];?></td>
												<td><?php print $current_item['name'];?></td>
												<td><?php print $current_item['date_of_birth'];?></td>
												<td><?php print $current_item['school'];?></td>
												<td><?php print $current_item['church'];?></td>
												<td><?php print $current_item['career'];?></td>
												<td><?php print $current_item['allergy'];?></td>
												<td><?php print $current_item['address'];?></td>
												<td><?php print $current_item['email'];?></td>
												<td><?php print $current_item['phone'];?></td>
												<td><button type="button" class="btn btn-primary btn-sm"
														data-toggle="modal" data-target="#myModal">Action</button>
												</td>
											</tr>
											<?php }}else{?>
											<tr class="danger">
												<td colspan="11" align="center"><strong>No Entries Found!</strong></td>
											</tr>
											<?php }?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- MODAL -->
			<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
				aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<form method="post">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal"
									aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
								<h4 class="modal-title" id="myModalLabel">Update Member Listing</h4>
							</div>

							<div class="modal-body">
								<div class="row">
									<div class="col-md-6">
										<div class="row">
											<div class="col-lg-12">
												<h4>Personal Information</h4>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12">
												<div class="input-group">
													<span class="input-group-addon"><i class="fa fa-user"></i></span>
													<input type="text" class="form-control"
														value="<?php echo $current_item['name'];?>"
														placeholder="Full Name" aria-describedby="basic-addon1"
														required autofocus name="txtname" id="txtname">
												</div>
												<br>
											</div>
											<div class="col-md-6">
												<div class="input-group">
													<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
													<input type="text" class="form-control"
														value="<?php echo $current_item['date_of_birth'];?>"
														placeholder="Date of Birth"
														aria-describedby="basic-addon1" required
														name="txtdate_of_birth" id="txtdate_of_birth">
												</div>
												<br>
												<div class="input-group">
													<span class="input-group-addon"><i class="fa fa-venus-mars"></i></span><select
														class="form-control" name="txtgender" id="txtgender">
														<option selected="selected" style="display: none;"><?php echo $current_item['gender'];?></option>
														<option>Male</option>

														<option>Female</option>
													</select>
												</div>
												<br>
												<div class="input-group">
													<span class="input-group-addon"><i class="fa fa-university"></i></span>
													<input type="text" class="form-control"
														value="<?php echo $current_item['school'];?>"
														placeholder="School" aria-describedby="basic-addon1"
														required name="txtschool" id="txtschool">
												</div>
												<br>
											</div>
											<div class="col-md-6">
												<div class="input-group">
													<span class="input-group-addon"><i class="fa fa-home"></i></span>
													<input type="text" class="form-control"
														value="<?php echo $current_item['church'];?>"
														placeholder="Church" aria-describedby="basic-addon1"
														required name="txtchurch" id="txtchurch">
												</div>
												<br>
												<div class="input-group">
													<span class="input-group-addon"><i class="fa fa-line-chart"></i></span>
													<input type="text" class="form-control"
														value="<?php echo $current_item['career'];?>"
														placeholder="Career/Ambition"
														aria-describedby="basic-addon1" required name="txtcareer"
														id="txtcareer">
												</div>
												<br>
												<div class="input-group">
													<span class="input-group-addon"><i class="fa fa-asterisk"></i></span>
													<input type="text" class="form-control"
														value="<?php echo $current_item['allergy'];?>"
														placeholder="Allergy" aria-describedby="basic-addon1"
														required name="txtallergy" id="txtallergy">
												</div>
												<br>
											</div>
											<div class="col-md-6">
												<div class="input-group">
													<span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
													<input type="text" class="form-control"
														value="<?php echo $current_item['address'];?>"
														placeholder="Physical Address"
														aria-describedby="basic-addon1" required name="txtaddress"
														id="txtaddress">
												</div>
												<br>
												<div class="input-group">
													<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
													<input type="email" class="form-control"
														value="<?php echo $current_item['email'];?>"
														placeholder="Email Address"
														aria-describedby="basic-addon1" required name="txtemail"
														id="txtemail">
												</div>
												<br>
											</div>
											<div class="col-md-6">
												<div class="input-group">
													<span class="input-group-addon"><i class="fa fa-phone"></i></span>
													<input type="text" class="form-control"
														value="<?php echo $current_item['phone'];?>"
														placeholder="Telephone Number"
														aria-describedby="basic-addon1" required name="txtphone"
														id="txtphone">
												</div>
												<br>
												<div class="input-group">
													<span class="input-group-addon"><i class="fa fa-phone"></i></span>
													<input type="text" class="form-control"
														value="<?php echo $current_item['other_phone'];?>"
														placeholder="Telephone Number (other)"
														aria-describedby="basic-addon1" required
														name="txtother_phone" id="txtother_phone">
												</div>
												<br>
											</div>
										</div>

									</div>
									<div class="col-md-6">
										<div class="row">
											<div class="col-md-12">
												<h4>Parent/Guardian Information</h4>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12">
												<div class="input-group">
													<span class="input-group-addon"><i class="fa fa-venus-mars"></i></span>
													<input type="text" class="form-control"
														value="<?php echo $current_item['parent_name'];?>"
														placeholder="Parent/Guardian Name"
														aria-describedby="basic-addon1" required
														name="txtparent_name" id="txtparent_name">
												</div>
												<br>
												<div class="input-group">
													<span class="input-group-addon"><i class="fa fa-phone"></i></span>
													<input type="text" class="form-control"
														value="<?php echo $current_item['parent_phone'];?>"
														placeholder="Telephone Number"
														aria-describedby="basic-addon1" required
														name="txtparent_phone" id="txtparent_phone">
												</div>
												<br>
												<div class="input-group">
													<span class="input-group-addon"><i class="fa fa-line-chart"></i></span>
													<input type="text" class="form-control"
														value="<?php echo $current_item['parent_career'];?>"
														placeholder="Career" aria-describedby="basic-addon1"
														required name="txtparent_career" id="txtparent_career">
												</div>
												<br>
												<div class="input-group">
													<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
													<input type="email" class="form-control"
														value="<?php echo $current_item['parent_email'];?>"
														placeholder="Parent/Guardian Email Address"
														aria-describedby="basic-addon1" required
														name="txtparent_email" id="txtparent_email">
												</div>
												<br>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-12">
												<h4>Coregroup Information</h4>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-6">
												<div class="input-group">
													<span class="input-group-addon"><i class="fa fa-signal"></i></span><select
														class="form-control" name="txtlevel" id="txtlevel">
														<option selected="selected" style="display: none;"><?php echo $current_item['level'];?></option>
																<?php
																include_once ("../classes/Levels.php");
																$current_levels = new Levels ();
																$current_list = $current_levels->getList ();
																if (sizeof ( $current_list ) > 0) {
																	foreach ( $current_list as $current_level ) {
																		?>
															<option><?php print $current_level['name'];?></option>
													<?php }}?>
														</select>
												</div>
												<br>
											</div>
											<div class="col-lg-6">
												<div class="input-group">
													<span class="input-group-addon"><i class="fa fa-group"></i></span><select
														class="form-control" name="txtgroup" id="txtgroup">
														<option selected="selected" style="display: none;"><?php echo $current_item['group_name'];?></option>
																<?php
																include_once ("../classes/Groups.php");
																$current_groups = new Groups ();
																$current_list = $current_groups->getList ();
																if (sizeof ( $current_list ) > 0) {
																	foreach ( $current_list as $current_item ) {
																		?>
															<option><?php print $current_item['name'];?></option>
													<?php }}?>
														</select>
												</div>
												<br>
											</div>
										</div>
									</div>

								</div>

							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary"
									data-dismiss="modal">Close</button>
								<button type="button" class="btn btn-danger"
									data-dismiss="modal">Delete</button>
								<button type="button" class="btn btn-success">Save changes</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /.container -->
<?php include_once ("common_files/javascript.php");?>

</body>

</html>
