<footer class="footer">
	<div class="container">
		<h5 class="text-muted">
			<b>Navigators Kenya Copyright &copy; <?= date('Y');?></b>
		</h5>
	</div>
</footer>