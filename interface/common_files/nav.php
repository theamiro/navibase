<?php
require_once ("session.php");
include_once ("../classes/Users.php");

$new_user = new Users ();
$username = $_SESSION ['user_session'];

$query = $new_user->execute_query ( "SELECT * FROM users where username = :username" );

$query->execute ( array (
		":username" => $username 
) );
?>

<!-- Navigation -->
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
	<div class="container">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="index.php">Navibase</a>
		</div>
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse navbar-right"
			id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li class="<?= ($activePage == 'members') ? 'active':''; ?>"><a
					href="members.php"><span><i class="fa fa-child"></i></span> Members</a></li>
				<li class="<?= ($activePage == 'attendance') ? 'active':''; ?>"><a
					href="attendance.php"><span><i class="fa fa-calendar-check-o"></i></span>
						Attendance</a></li>
				<li class="<?= ($activePage == 'levels') ? 'active':''; ?>"><a
					href="levels.php"><span><i class="fa fa-signal"></i></span> Levels</a></li>
				<li class="<?= ($activePage == 'groups') ? 'active':''; ?>"><a
					href="groups.php"><span><i class="fa fa-group"></i></span> Groups</a></li>
				<li class="<?= ($activePage == 'mailing') ? 'active':''; ?>">
				
				<li class="<?= ($activePage == 'users') ? 'active':''; ?>"><a
					href="users.php"><span><i class="fa fa-plus"></i></span> New User</a></li>
				<li class="<?= ($activePage == 'logout') ? 'active':''; ?>"><a
					href="#" class="dropdown-toggle" data-toggle="dropdown"
					role="button" aria-haspopup="true" aria-expanded="true"><span><i
							class="fa fa-user"></i> </span> <span><i class="fa fa-caret-down"></i></span></a>
					<ul class="dropdown-menu">
						<li><a> <span><i class="fa fa-user"></i></span>  <?= $username;?>
						</a></li>
						<li role="separator" class="divider"></li>
						<li><a href="logout.php?logout=true">Logout<span
								class="pull-right"><i class="fa fa-sign-out"></i></span></a></li>
					</ul></li>
			</ul>
		</div>
		<!-- /.navbar-collapse -->
	</div>
	<!-- /.container -->
</nav>