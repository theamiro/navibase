<?php
require_once ("session.php");
require_once ("../classes/Users.php");
$user_logout = new Users ();

if ($user_logout->is_Loggedin () != "") {
	$user_logout->redirect ( 'members.php' );
}
if (isset ( $_GET ['logout'] ) && $_GET ['logout'] == "true") {
	$user_logout->logout ();
	$user_logout->redirect ( 'index.php' );
}
?>